import numpy as np
import matplotlib.pyplot as plt

class  InverseEffectCalc:
    def __init__(self):
        self.default_l = -0.025
        self.delta_l = 0.005
        self.u_with_line = 1

    def inputVars(self):
        a1 = float(input('a1 for matrix A: '))
        a2 = float(input('a2 for matrix A: '))
        t0 = float(input('Quantization period t0: '))
        q = int(input('Accuracy q: '))
        var = [a1, a2, t0, q]
        return var
    
    def draw(self, t, list_y1, list_y1_opti):
        plt.xlabel('t')
        plt.ylabel('x1(t)')
        plt.grid()
        plt.xticks(np.arange(t[0], t[-1]+1, 5))
        plt.plot(t, list_y1, label='not optimized', color='red')
        plt.plot(t, list_y1_opti, label='optimized', color='blue')
        plt.legend()
        plt.show()

    def initA(self, a1, a2):
        a = np.zeros((3, 3))
        a[0][1] = 1
        a[1][2] = 1
        a[2][0] = -1
        a[2][1] = -a1
        a[2][2] = -a2
        return a

    def initB(self):
        b = np.zeros((3, 1))
        b[0][0] = 0
        b[1][0] = 0
        b[2][0] = 1
        return b

    def initC(self):
        c = []
        for i in range(3):
            value = np.zeros((1, 3))
            value[0][i] = 1
            c.append(value)
        return c

    def countFirstEq(self, f, x, g, l):
        u_k = -l.transpose().dot(x) + self.u_with_line
        res = f.dot(x) + g*u_k
        return res

    def countSecondEq(self, x, c, y):
        y.append(c.dot(x))
        return y

    def countF(self, a, t, q):
        f = np.eye(3)
        try:
            if isinstance(q, int) == 0:
                raise Exception('This accuracy isn`t integer')
            elif q < 2 or q > 10:
                raise Exception("This accuracy isn`t allowed")
        except Exception as e:
            print(e)
            return 0
        for i in range(1, q+1):
            f += np.linalg.matrix_power(a.dot(t), i) / np.math.factorial(i)
        return f

    def countG(self, a, q, t0):
        b = self.initB()
        e = np.eye(3)
        tmp = e*t0
        for j in range(2, q+1):
            tmp += (np.linalg.matrix_power(a, j-1).dot(e)*(t0**j))/np.math.factorial(j)

        return tmp.dot(b)

    def countJ(self, x, t0):
        sum = 0.000000000000
        for i in range(len(x)):
            sum += abs(x[i][0][0] - 1)*t0

        return sum

    def quantizationOptimisation(self, f, g, x, t, ll, t0, tmp):
        x0 = [x]
        for i in range(len(t)):
            x0.append(self.countFirstEq(f, x0[i], g, ll))
        j0 = self.countJ(x0, t0)
        flag = False
        print('J begin ', j0)
        ll[tmp][0] += self.delta_l
        xn = [x]
        for i in range(len(t)):
            xn.append(self.countFirstEq(f, xn[i], g, ll))
        j_i = self.countJ(xn, t0)
        while j0-j_i > 0:
            xn.clear()
            xn = [x]
            ll[tmp][0] += self.delta_l
            for i in range(len(t)):
                xn.append(self.countFirstEq(f, xn[i], g, ll))
            j_i = self.countJ(xn, t0)
            flag = True

        print('opti', ll[tmp][0])
        if flag is False:
            ll[tmp][0] = self.default_l
        while j0-j_i <= 0 and flag is False:
            xn.clear()
            xn = [x]
            ll[tmp][0] -= self.delta_l
            for i in range(len(t)):
                xn.append(self.countFirstEq(f, xn[i], g, ll))
            j_i = self.countJ(xn, t0)
        print('opti', ll[tmp][0])
        return xn

    def runInterface(self):
        val = self.inputVars()
        x = np.zeros((3, 1))
        t = list(np.arange(0, 30+val[2], val[2]))
        a = self.initA(val[0], val[1])
        f = self.countF(a, val[2], val[3])
        g = self.countG(a, val[3], val[2])
        l = np.array([[0.0],
                    [0.0],
                    [0.0]])
        tmp = int(input("Input variant 1 or 2: "))
        if tmp != 1 and tmp != 2:
            print("Input isn`t 1 or 2")
            return 0
        l[tmp][0] = self.default_l
        x_n = []
        x_n.append(x)
        for i in range(len(t)):
            x_n.append(self.countFirstEq(f, x_n[i], g, l))
        l[tmp][0] = 0.0
        x_n_optimazed = self.quantizationOptimisation(f, g, x, t, l, val[2], tmp)
        c = self.initC()
        y1 = []
        y1_opti = []
        for i in x_n:
            y1 = self.countSecondEq(i, c[0], y1)
        for i in x_n_optimazed:
            y1_opti = self.countSecondEq(i, c[0], y1_opti)
        lol1 = []
        for i in y1:
            a = i.tolist()
            lol1.append(a[0])
        list_y1 = []
        for m in range(len(lol1)-1):
            list_y1.append(lol1[m][0])
        lol2 = []
        for i in y1_opti:
            a = i.tolist()
            lol2.append(a[0])
        list_y1_opti = []
        for m in range(len(lol2) - 1):
            list_y1_opti.append(lol2[m][0])

        self.draw(t, list_y1, list_y1_opti)


if __name__ == '__main__':
    calc = InverseEffectCalc()
    calc.runInterface()