import numpy as np
import math
import matplotlib.pyplot as plt
import tabulate

class SystemTransients_Type1:
    def __init__(self, a1, a2, b, T0, q):
        self.initA(a1, a2)
        self.initB(b)
        self.initC()        
        self.T0 = T0
        self.initF(q)
        self.initG()
    
    def initA(self, a1, a2):
        self.A = np.matrix([
                [0, 1, 0],
                [0, 0, 1],
                [-1, -a1, -a2]
        ], dtype = float)
    
    def initB(self, b):
        self.B = np.matrix([
                [0],
                [0],
                [b]
        ], dtype = float)

    def initC(self):
        self.C = np.matrix([
                [1, 0, 0]
        ], dtype = float)

    def initF(self, q):
        self.F = np.eye(3)
        for i in range(1, q+1):
            self.F += np.linalg.matrix_power(self.A * self.T0, i) / math.factorial(i)

    def initG(self):
        self.G = (self.F - np.eye(3)).dot(np.linalg.inv(self.A)).dot(self.B)

    def initStartArray(self, x1, x2, x3):
        startArray = np.matrix([
                [x1],
                [x2],
                [x3]
        ], dtype = float)
        return startArray
    
    def calculate(self, iter = 500, x1 = 0, x2 = 0, x3 = 0):
        u = 1
        nextX = self.F.dot(self.initStartArray(x1, x2, x3)) + (self.G * u)
        array_of_vectors = []
        arrayX = []
        arrayY = []
        for i in range(iter):
            u = self.getU(i, iter)
            prevX = nextX
            nextX = self.F.dot(prevX) + (self.G * u)
            arrayY.append(float(self.C.dot(nextX)))
            array_of_vectors.append(nextX)
            arrayX.append(i * self.T0)
        self.convertForVisualize(array_of_vectors, iter)

    def convertForVisualize(self, array_of_vectors, iter):
        self.timeline = np.linspace(0, iter * self.T0, iter)
        self.x1, self.x2, self.x3 = [], [], []
        for i in range(iter):
            self.x1.append(float(array_of_vectors[i][0]))
            self.x2.append(float(array_of_vectors[i][1]))
            self.x3.append(float(array_of_vectors[i][2]))

    def visualize(self):
        plt.plot(self.timeline, self.x1, label = 'x1')
        plt.plot(self.timeline, self.x2, label = 'x2')
        plt.plot(self.timeline, self.x3, label = 'x3')
        plt.legend()
        plt.show()

    def table(self):        
        table = []
        iterator = int(len(self.timeline) / int(max(self.timeline))) - 1
        i = 0
        while i < len(self.timeline):
            table.append([round(self.timeline[i]), self.x1[i], self.x2[i], self.x3[i]])
            i += iterator
        print(tabulate.tabulate(table, headers = ['k', 'x1', 'x2', 'x3']), end = '\n\n')

    def getU(self, i, iter):
        return 1

class SystemTransients_Type2(SystemTransients_Type1):
    def getU(self, i, iter):
        if i >= int(iter / 2):
            return -1
        return 1

class SystemTransients_Type3(SystemTransients_Type1):
    def getU(self, i, iter):
        if i >= int(iter - int(iter / 3)):
            return 1
        if i >= int(iter / 3):
            return -1
        return 1
    
if __name__ == '__main__':
    system = SystemTransients_Type1(1, 3, 1, 0.02, 10) # a1, a2, b, T0, q
    system.calculate(500, 0, 0, 0) # iter, x1_start, x2_start, x3_start
    system.table() # print table
    system.visualize() # draw graph
    
    system = SystemTransients_Type2(1, 3, 1, 0.02, 10) # a1, a2, b, T0, q
    system.calculate(500) # iter
    system.table() # print table
    system.visualize() # draw graph

    system = SystemTransients_Type3(1, 3, 1, 0.02, 10) # a1, a2, b, T0, q
    system.calculate(500) # iter
    system.table() # print table
    system.visualize() # draw graph