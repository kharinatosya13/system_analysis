# System_Analysis


Numerical methods for studying transition process for
by a given input u(t) for a system whose mathematical model has
appearance:

x ● (t) = Ax(t) + Bu(t), 

t is [0, ∞); - equation of state evolution
systems in differential form

y(t) = Cx(t); - system output equation

x(t) is R n – the system state vector, 

u(t) is R m – the input variable, 

y(t) is R l –
output variable, A is R n*n, B is R n*m, C is R l*n – parameter matrices
systems